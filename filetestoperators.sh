#! /bin/bash

echo -e "Enter your file name : \c"
read file_name

#-e for exsits or not, -d for directory, -b for block specific file(video, audio, image), 
# -c for text,data file)
# -s is for empty or not
# -r for read permission or not and -w for write
if [ -e $file_name  ]
echo "$file_name exists"
else
echo "$file_name not exists"
fi
